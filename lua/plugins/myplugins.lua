return {

  {
    "neovim/nvim-lspconfig",
    config = function()
      require("nvchad.configs.lspconfig").defaults()
      require "configs.lspconfig"
    end,
  },

  {
    "williamboman/mason.nvim",
    opts = {
      ensure_installed = { "html-lsp", "clangd", "clang-format", "rust-analyzer", "codelldb", "ansible-language-server", "ansible-lint", "lua-language-server", "yaml-language-server" }
    }
  },

  {
    "stevearc/aerial.nvim",
    lazy = false,
    config = function()
      require('aerial').setup({
        -- optionally use on_attach to set keymaps when aerial has attached to a buffer
        on_attach = function(bufnr)
          -- Jump forwards/backwards with '{' and '}'
          vim.keymap.set('n', '{', '<cmd>AerialPrev<CR>', {buffer = bufnr})
          vim.keymap.set('n', '}', '<cmd>AerialNext<CR>', {buffer = bufnr})
        end
      })
    end,
  },

  { "folke/which-key.nvim", lazy = true },

  {
    "nvim-treesitter/nvim-treesitter",
    opts = {
      ensure_installed = {
        "html",
        "css",
        "javascript",
        "typescript",
        "python",
        "json",
        "c",
        "rust",
        "java",
        "c_sharp",
        "yaml",
      },

      highlight = {
        vim.api.nvim_set_hl(0, "@comment.documentation.rust", { fg = "#89dceb" })
      },
    },
  },
  { "nvim-treesitter/nvim-treesitter-context", lazy = false },

  {
    'mfussenegger/nvim-dap',
    config = function()
      require "configs.dap"
    end,
    requires = {
      "Pocco81/DAPInstall.nvim",
      --"mfussenegger/nvim-dap-python", can be used instead of a config file for the Python adapter by requiring it where your config would be.
    },
  },

  {
    "rcarriga/nvim-dap-ui",
    after = "nvim-dap",
    config = function()
      require("dapui").setup()
    end,
  },

  {
    "rust-lang/rust.vim",
    ft = "rust",
    init = function()
      vim.g.rustfmt_autosave = 1
    end
  },

  -- {
  --   "simrat39/rust-tools.nvim",
  --   ft = "rust",
  --   dependencies = "neovim/nvim-lspconfig",
  --   opts = function()
  --     return require "configs.rust-tools"
  --   end,
  --   config = function(_, opts)
  --     require('rust-tools').setup(opts)
  --   end
  -- },

  {
    "christoomey/vim-tmux-navigator",
    lazy = false
  },

  {
    "saecki/crates.nvim",
    dependencies = "hrsh7th/nvim-cmp",
    ft = {"rust", "toml"},
    config = function(_, opts)
      local crates = require("crates")
      crates.setup(opts)
      crates.show()
    end,
  },

  {
    "hrsh7th/nvim-cmp",
    opts = function()
      local M = require "nvchad.configs.cmp"
      table.insert(M.sources, {name = "crates"})
      return M
    end,
  },

  {
    "pearofducks/ansible-vim",
    ft = {"yaml", "yaml.ansible"}
  },

  {
    "tyrossel/MarkdownTable.nvim",
    ft = {"markdown"},
  },

  {
    'nvim-lualine/lualine.nvim',
    dependencies = { 'nvim-tree/nvim-web-devicons' },
    event = "VeryLazy",
    opts = {
      options = {
        icons_enabled = true,
        theme = 'auto',
        component_separators = '|',
        section_separators = { left = '', right = '' },
      },
    },
  },

  { "nvim-neotest/nvim-nio" },

}

-- First read our docs (completely) then check the example_config repo

local M = {}

M.ui = {
  theme = "catppuccin",
  lsp_semantic_tokens = false, -- not merged already, needs #af123ee

  statusline = {
    theme = "vscode_colored",
  },
}

-- M.plugins = "plugins"

-- M.mappings = require "mappings"

return M

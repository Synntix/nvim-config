require "nvchad.mappings"

local map = vim.keymap.set

map("n", "<C-h>", "<cmd> TmuxNavigateLeft<CR>", {desc = "window left"})
map("n", "<C-j>", "<cmd> TmuxNavigateDown<CR>", {desc = "window down"})
map("n", "<C-k>", "<cmd> TmuxNavigateUp<CR>", {desc = "window up"})
map("n", "<C-l>", "<cmd> TmuxNavigateRight<CR>", {desc = "window right"})

map("n", "<leader>a", "<cmd>AerialToggle!<CR>", {desc = "toggle outline"})

map("n", "<leader>db", '<Cmd>lua require"dapui".toggle()<CR>', {desc = "Toggle DAP UI"})


-- M.dap = {
--   n = {
--     ["<Leader>d"] = {'<Cmd>lua require"dapui".toggle()<CR>', "Toggle DAP UI"},
--     ["<A-b>"] = {'<Cmd>lua require"dap".toggle_breakpoint()<CR>', "Toggle breakpoint"},
--     ["<A-c>"] = {'<Cmd>:lua require"dap".continue()<CR>', "DAP continue"},
--     ["<A-s>"] = {'<Cmd>lua require"dap".step_over()<CR>', "DAP step over"},
--     ["<A-S>"] = {'<Cmd>lua require"dap".step_into()<CR>', "DAP step into"},
--   }
-- }

map(
  "n",
  "<leader>rcu",
  function()
    require("crates").upgrade_all_crates()
  end,
  {desc = "update all crates"}
)

map("n", "<A-f>", ":Telescope fd <CR>", {desc = "Telescope Files"})
map("n", "<S-R>", "<Cmd>lua require('nvterm.terminal').send('cargo run' .. '\\r', 'float')<CR>", {desc = "Run cargo run in terminal"})

return M
